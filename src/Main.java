import java.util.Scanner;

public class Main {

    private Scanner sc = new Scanner(System.in);

    public void showMenu(){
        System.out.println("Escoge una opcion:");
        System.out.println("1.Hello World");
        System.out.println("0. Salir\n");
    }

    public int readOpcion() {
        return sc.nextInt();
    }

    public void operar(int opcion){

        switch (opcion){
            case 0:
                exit();
                break;
        }
    }

    /* OPERATIONS */
    public void exit(){
        int vector1[]=new int[3];
        int vector2[]=new int[2];


        int i;
        for (i=0; i<vector1.length; i++) vector1[i] = sc.nextInt();
        for (i=0; i<vector2.length; i++) vector2[i] = sc.nextInt();

        int vector3[]=new int[vector1.length+vector2.length];
        System.out.println(vector3);

        System.arraycopy(vector1[i],0,vector3,0,vector1.length);
        System.arraycopy(vector2,0,vector3,vector1.length, vector2.length);
        System.out.println("Valor de la FUSIO dels dos arrays "+vector3+"");
    }

    public static void main(String[] args) {

        Main Main = new Main();

        int response = 0;
        do{
            Main.showMenu();
            response = Main.readOpcion();
            Main.operar(response);
        }while(response != 0);

    }
}